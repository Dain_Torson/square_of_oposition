

def contrary(x):
    return False if x else None


def subcontrary(x):
    return True if not x else None


def contradiction(x):
    return not x


def submission(x):
    return x if x else None


def reversed_submission(x):
    return x if not x else None


class Square:

    def __init__(self):
        self._A = None
        self._E = None
        self._i = None
        self._o = None
        self._prev_operation = "No operations were performed"

    def __str__(self):
        return self._prev_operation + "\n" + \
               "A: " + str(self._A) + " E: " + str(self._E)\
               + " i: " + str(self._i) + " o: " + str(self._o) + "\n"

    def setA(self, value):
        value = bool(value)
        self._A = value
        self._E = contrary(value)
        self._i = submission(value)
        self._o = contradiction(value)
        self._prev_operation = "A was set to: " + str(value)

    def setE(self, value):
        value = bool(value)
        self._A = contrary(value)
        self._E = value
        self._i = contradiction(value)
        self._o = submission(value)
        self._prev_operation = "E was set to: " + str(value)

    def setI(self, value):
        value = bool(value)
        self._A = reversed_submission(value)
        self._E = contradiction(value)
        self._i = value
        self._o = subcontrary(value)
        self._prev_operation = "i was set to: " + str(value)

    def setO(self, value):
        value = bool(value)
        self._A = contradiction(value)
        self._E = reversed_submission(value)
        self._i = subcontrary(value)
        self._o = value
        self._prev_operation = "o was set to: " + str(value)